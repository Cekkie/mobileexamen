package tasks.TaskListTasks;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;

import activity.ActivityFindTLbyIdInterface;
import activity.EditTaskListDetailActivity;
import database.DbTaskListInterface;
import database.DbTaskListSQL;
import domain.TaskList;

/**
 * Created by Thul on 29/12/2015.
 */


public class GetTlByIdTask extends AsyncTask<Void, Void, Boolean> {

    private DbTaskListInterface db;
    private TaskList res;
    private ActivityFindTLbyIdInterface context;

    public GetTlByIdTask(ActivityFindTLbyIdInterface context) {
        this.context = context;
    }


    @Override
    protected Boolean doInBackground(Void... params) {
        try {


            db = new DbTaskListSQL();
            res =  db.getTaskList(context.getId());

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean succes) {
        if(succes) {
           context.onSucces(res);
        } else {

          context.onFail("Het lukt alweer niet");
        }
    }
}
