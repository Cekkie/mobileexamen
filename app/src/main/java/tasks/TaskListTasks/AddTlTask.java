package tasks.TaskListTasks;

import android.os.AsyncTask;

import database.DbTaskListInterface;
import database.DbTaskListSQL;
import domain.TaskList;

/**
 * Created by Thul on 29/12/2015.
 */
public class AddTlTask extends AsyncTask<TaskList, Void, Void> {



    @Override
    protected Void doInBackground(TaskList... tl) {
        DbTaskListInterface db = new DbTaskListSQL();
        db.addTaskList(tl[0]);
        return null;
    }
}
