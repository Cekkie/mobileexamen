package tasks.TaskListTasks;

import android.os.AsyncTask;

import activity.ActivityRemoveTaskListIdInterface;
import database.DbTaskListInterface;
import database.DbTaskListSQL;
import domain.TaskList;

/**
 * Created by Thul on 29/12/2015.
 */
public class RemoveTlByIdTask extends AsyncTask <Void, Void, Boolean> {
    DbTaskListInterface db;
    TaskList taskList;
    private ActivityRemoveTaskListIdInterface context;

    public RemoveTlByIdTask(ActivityRemoveTaskListIdInterface context) {
        this.context = context;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            db = new DbTaskListSQL();
            db.removeTaskList(context.getId());
            return true;
        } catch( Exception e ) {
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean succes) {
        if (succes) {
            context.onSuccesRemoveTaskListId();
        } else {
            context.onFailRemoveTaskListId("het faalde");
        }
    }
}
