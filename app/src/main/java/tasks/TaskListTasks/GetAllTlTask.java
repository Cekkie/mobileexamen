package tasks.TaskListTasks;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import java.util.List;

import activity.EditTaskListDetailActivity;
import activity.MainActivity;
import activity.TaskListDetailActivity;
import database.DbTaskListInterface;
import database.DbTaskListSQL;
import domain.Task;
import domain.TaskList;

/**
 * Created by Thul on 29/12/2015.
 */
public class GetAllTlTask extends AsyncTask<Void ,Void,Boolean> {

    //private EditTaskListDetailActivity context;
    List<TaskList> res;
    DbTaskListInterface db;
    MainActivity activity;

    public GetAllTlTask(MainActivity context) {
        activity =  context;
    }


    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            db = new DbTaskListSQL();
            res = db.getTaskLists();
            return true;
        } catch (Exception e) {
            System.out.println("Exception e");
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean succes) {
        if(succes) {
            activity.taskSucces(res);
        } else {
            activity.taskFail("Het lukt alweer niet");
        }
    }
}
