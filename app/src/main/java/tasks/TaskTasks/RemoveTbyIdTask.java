package tasks.TaskTasks;

import android.os.AsyncTask;

import java.util.List;

import activity.ActivityRemoveTaskByIdInterface;
import database.DbTaskInterface;
import database.DbTaskListInterface;
import database.DbTaskListSQL;
import database.DbTaskSQL;
import domain.Task;
import domain.TaskList;

/**
 * Created by Thul on 30/12/2015.
 */
public class RemoveTbyIdTask extends AsyncTask<Void, Void, Boolean> {
    private DbTaskInterface db;
    private DbTaskListInterface db_Tl;
    private ActivityRemoveTaskByIdInterface context;

    public RemoveTbyIdTask(ActivityRemoveTaskByIdInterface context) {
        this.context = context;

    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            db = new DbTaskSQL();
            db_Tl = new DbTaskListSQL();
            Task task = db.getTaskById(context.getId());
            db.removeTask(context.getId());
            TaskList tl = task.getTaskList();
            boolean res = true;
            if(db.getTaskList(tl.getId().intValue()).size() <=0) {
                res = false;
            }
            for(Task t : db.getTaskList(tl.getId().intValue())) {
                if (!t.getisChecked()) {
                    res = false;
                    break;
                }
            }

            tl.setIsDone(res);
            db_Tl.updateTaskList(tl);

            return true;
        } catch(Exception e) {
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean succes) {
        if(succes) {
            context.onSuccesRemoveTaskById();
        } else {
            context.onFailRemoveTaskById("weinig");
        }


    }
}
