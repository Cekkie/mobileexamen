package tasks.TaskTasks;

import android.os.AsyncTask;

import database.DbTaskInterface;
import database.DbTaskSQL;
import domain.Task;

/**
 * Created by Thul on 30/12/2015.
 */
public class AddTtask extends AsyncTask<Task, Void, Void>{

    @Override
    protected Void doInBackground(Task... params) {
        DbTaskInterface db = new DbTaskSQL();
        db.addTask(params[0]);
        return null;
    }

}
