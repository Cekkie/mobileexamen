package activity;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.example.simon.mobileproject.R;

import domain.Task;
import domain.TaskList;
import tasks.TaskListTasks.AddTlTask;
import tasks.TaskListTasks.GetAllTlTask;
import tasks.TaskListTasks.GetTlByIdTask;
import tasks.TaskTasks.AddTtask;
import tasks.TaskTasks.GetAllTByListIdTask;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class AddTaskActivity extends AppCompatActivity implements ActivityFindTLbyIdInterface, ActivityFindTByListIdInterface
{

    private EditText subject;
    private EditText description;
    private Context context;
    private int position;
    private TaskList list;
    private List<Task> tasks;
    private Task t;
    private ActivityFindTByListIdInterface activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActiveAndroid.initialize(this);
        setContentView(R.layout.activity_add_task);
        context = this;
        activity = this;
        readIntentExtra();

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Write your logic here
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void readIntentExtra(){
        Bundle extras = getIntent().getExtras();
        position = extras.getInt("taskListId");
        new GetTlByIdTask(this).execute();
    }

    @Override
    public void onSucces(TaskList res) {
        list = res;
        System.out.println("----------------" + res);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.save_task);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subject = (EditText) findViewById(R.id.subject);
                description = (EditText) findViewById(R.id.description);

                t = new Task();
                t.setSubject(subject.getText().toString());
                t.setDescription(description.getText().toString());
                t.setTaskList(list);

                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                t.setCreationDate(df.format(c.getTime()));


                new AddTtask().execute(t);
                new GetAllTByListIdTask(activity).execute(list.getId().intValue());

            }
        });
    }

    @Override
    public void onFail(String res) {
        System.out.println(res);
    }

    @Override
    public int getId() {
        return position;
    }

    @Override
    public void onSuccesFindTbyListId(List<Task> res) {
        tasks = res;

        tasks.add(t);
        list.setIsDone(false);

        new AddTlTask().execute(list);

        Intent i = new Intent(context, TaskOverviewActivity.class);
        i.putExtra("taskListId", position);
        startActivity(i);
    }

    @Override
    public void onFailFindTByListId(String res) {

    }
}
