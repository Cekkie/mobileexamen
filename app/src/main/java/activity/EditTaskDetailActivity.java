package activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.activeandroid.ActiveAndroid;
import com.example.simon.mobileproject.R;

import domain.Task;
import tasks.TaskTasks.AddTtask;
import tasks.TaskTasks.GetTbyId;

public class EditTaskDetailActivity extends AppCompatActivity implements AcitivityFindTbyIdInterface {

    private EditText name;
    private EditText description;
    private Context context;
    private Task task;
    private Button save;
    private int position;
    private EditTaskDetailActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActiveAndroid.initialize(this);
        setContentView(R.layout.activity_edit_task_detail);
        context = this;
        activity = this;
        readData();


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);




    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Write your logic here
                this.finish();
                startActivity(new Intent(this, MainActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initializeUiComponents(){
        name = (EditText)findViewById(R.id.editDetailName);
        description = (EditText)findViewById(R.id.editDetailDescription);
        name.setHint(task.getSubject());
        description.setHint(task.getDescription());
    }

    private void readData(){
        Bundle extras = getIntent().getExtras();
        position = extras.getInt("task");
        new GetTbyId(activity).execute();
     //   task = Task.load(Task.class, position);
    }

    @Override
    public void onSuccesFindTbyId(Task t) {
        task = t;

        initializeUiComponents();
        save = (Button)findViewById(R.id.editSaveTask);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                task.setSubject((name.getText().toString().isEmpty()) ? task.getSubject() : name.getText().toString());
                task.setDescription((description.getText().toString().isEmpty()) ? task.getDescription() : description.getText().toString());
                new AddTtask().execute(task);

                Intent intent = new Intent(context, TaskDetailActivity.class);
                intent.putExtra("task", task.getId().intValue());
                startActivity(intent);
            }
        });
    }

    @Override
    public void onFailFindTbyId(String res) {
        System.out.println(res);
    }

    @Override
    public int getTaskId() {
        return this.position;
    }
}
