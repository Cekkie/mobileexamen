package activity;

import domain.Task;

/**
 * Created by Thul on 30/12/2015.
 */
public interface AcitivityFindTbyIdInterface {
    public void onSuccesFindTbyId(Task t);
    public void onFailFindTbyId(String res);
    public int getTaskId();
}
