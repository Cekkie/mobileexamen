package domain;

import domain.TaskList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by simon on 11/25/2015.
 */
public class TaskListManager {
    private List<TaskList> manager;


    public TaskListManager(){
        manager = new ArrayList<TaskList>();
    }

    public TaskListManager(List<TaskList> list){
        this.manager = list;
    }



    public void addTaskList(TaskList list){
        manager.add(list);
    }

    public void removeTaskList(int id){
        for(TaskList l : manager){
            if(l.getId() == id){
                manager.remove(l);
            }
        }
    }

    public List<TaskList> gettTaskLists(){
        return manager;
    }


}
