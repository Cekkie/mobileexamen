package database;

import java.util.List;

import domain.Task;
import domain.TaskList;

/**
 * Created by Thul on 29/12/2015.
 */
public interface DbTaskInterface {

    public List<Task> getTaskList(int id);
    public void addTask (Task task);
    public void removeTask(int id);
    public void updateTask(Task task);
    public Task getTaskById(int id);
}
